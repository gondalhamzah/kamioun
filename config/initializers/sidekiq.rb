uri = URI.parse(ENV['REDIS_URL'])
Sidekiq.configure_server do |config|
  config.redis = { :host => uri.host, :port => uri.port, :password => uri.password, :scheme => uri.scheme }
end

Sidekiq.configure_client do |config|
  config.redis = { :host => uri.host, :port => uri.port, :password => uri.password, :scheme => uri.scheme }
end
