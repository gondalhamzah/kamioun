Rails.application.routes.draw do
  get 'super_users/dashboard'

  get 'clients/dashboard'
  get "clients/client_info" => 'clients#trans_info', :as => :trans_info
  
  resources :freight_missions

  resources :trips

  resources :clients do
  end

  get 'transporters/dashboard'
  get "transporters/client_info" => 'transporters#client_info', :as => :client_info


  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  get 'welcome/index'

  # root 'welcome#index'
  devise_scope :user do
    root to: "devise/sessions#new"
  end
end
