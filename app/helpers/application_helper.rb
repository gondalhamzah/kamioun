module ApplicationHelper
  
  def body_style
    "home-img" if params[:controller] == "devise/sessions" && (params[:action] == "new" || params[:action] == "create")
  end

end
