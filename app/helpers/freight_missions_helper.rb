module FreightMissionsHelper

  def is_mission_new?
    true if params[:controller] == "freight_missions" and (params[:action] == "new" or params[:action] == "create")
  end
  
end
