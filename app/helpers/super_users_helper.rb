module SuperUsersHelper
  def date_format(date)
    date.strftime("%d/%m")
  end
end
