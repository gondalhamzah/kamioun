module TripsHelper
  def is_trip_new?
    true if params[:controller] == "trips" and (params[:action] == "new" or params[:action] == "create")
  end
end
