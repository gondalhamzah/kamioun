class SendSmsWorker
  include Sidekiq::Worker

  def perform(*args)
    User.super_users_or_transporters.each do |t|
      if t && t.phone
        begin
          TWILIO_CLIENT.api.account.messages.create(
            from: ENV['TWILIO_PHONE_NUMBER'],
            to: t.phone,
            body: 'Un client vient de poster une nouvelle mission: http://kamioun.com'
          )
        rescue Twilio::REST::RestError => e
          puts e.message
        end
      end  
    end  
  end
end
