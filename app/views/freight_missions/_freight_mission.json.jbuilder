json.extract! freight_mission, :id, :departure_city, :arrival_city, :type_of_goods, :dimensions, :pick_up_date, :pick_up_time, :delivery_date, :delivery_time, :specific_details, :client_id, :created_at, :updated_at
json.url freight_mission_url(freight_mission, format: :json)
