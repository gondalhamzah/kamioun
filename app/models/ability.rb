class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    else
      can :read, :all
    end

    user = current_user

    if not user.nil?
      if user.client?
        can :dashboard, Client     
        can :create, FreightMission     
        can :index, FreightMission     
        can :edit, FreightMission     
        can :update, FreightMission     
        can :new, FreightMission     
        cannot :dashboard, Transporter
        cannot :dashboard, SuperUser

      elsif user.transporter?
        cannot :dashboard, Client
        can :dashboard, Transporter
        can :index, FreightMission       
        cannot :show, FreightMission
        cannot :edit, FreightMission     
        cannot :update, FreightMission     
        cannot :new, FreightMission     
        cannot :show, FreightMission
        cannot :dashboard, SuperUser

        elsif user.super_user?
        can :dashboard, SuperUser     
        can :create, FreightMission     
        can :index, FreightMission     
        can :edit, FreightMission     
        can :update, FreightMission     
        can :new, FreightMission     
        cannot :dashboard, Transporter
        cannot :dashboard, Client
                
      end
    end

  end
end