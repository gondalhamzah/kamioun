class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :email, presence: true
  validates :password, presence: true

  has_many :freight_missions
  has_many :trips
  
  #Get methods for UserType
  def self.client
    "Client"
  end
  
  def self.super_user
    "SuperUser"
  end
  
  def self.transporter
    "Transporter"
  end
  
  def client?
    self.type.eql? "Client" 
  end  
  
  def super_user?
    self.type.eql? "SuperUser" 
  end

  def transporter?
    self.type.eql? "Transporter"
  end

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def self.super_users_or_transporters
    where("type = ? OR type = ?","Transporter", "SuperUser")
  end

end