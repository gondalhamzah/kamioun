class TransportersController < ApplicationController
  before_action :authenticate_user!
  
  def dashboard
    @freight_missions = FreightMission.order("created_at desc")
    @trips = current_user.trips.order("created_at desc")
  end

  def client_info
    @fmission = FreightMission.find(params[:fmission])
    respond_to do |format|
      format.html
      format.js
    end
  end
  
end
