class SuperUsersController < ApplicationController
  before_action :authenticate_user!#, :except => [:show, :index]

  def dashboard
    @user_missions = current_user.freight_missions.order("created_at desc")
    @all_missions = FreightMission.all.order("created_at desc")
    @user_trips = current_user.trips.order("created_at desc")
    @all_trips = Trip.all.order("created_at desc")
  end
end