class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

 def after_sign_in_path_for(resource)

    if params[:admin_user]
      admin_root_path
    else
      if current_user.client?
        clients_dashboard_path
      elsif current_user.super_user?
        super_users_dashboard_path
      else
        transporters_dashboard_path
      end
    end
  
  end
  
  rescue_from CanCan::AccessDenied do |exception|
      
      # if not current_user.nil? 
      #   user = current_user
      #   if user.type.eql? User.doctor || current_user.doctor?
      #     redirect_to doctorsdashboard_path
      #   elsif user.type.eql? User.patient  || current_user.patient?
      #     redirect_to patientsdashboard_path
      #   elsif user.type.eql? User.assistant || current_user.assistant?
      #     redirect_to assistantsdashboard_path
      #   end
      # else
      #     redirect_to new_user_session_path
      # end
      redirect_to new_user_session_path
  end


end
