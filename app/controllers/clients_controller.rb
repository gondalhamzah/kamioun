class ClientsController < ApplicationController
  before_action :authenticate_user!#, :except => [:show, :index]
  
  def dashboard
    @freight_missions = current_user.freight_missions.order("created_at desc")
    @trips = Trip.order("created_at desc")
  end

  def trans_info
    @ftrip = Trip.find(params[:ftrips])
    respond_to do |format|
      format.html
      format.js
    end
  end
end
