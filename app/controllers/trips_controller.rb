class TripsController < ApplicationController
  before_action :set_trip, only: [:show, :edit, :update, :destroy]

  def index
    @trips = current_user.trips
  end

  def show
  end

  def new
    @trip = Trip.new
  end

  def edit
  end

  def create
    @trip = current_user.trips.new(trip_params)
    respond_to do |format|
      if @trip.save
        # SendSmsWorker.perform_async
        if current_user.super_user?
          format.html { redirect_to super_users_dashboard_path, notice: 'Trip was successfully created.' }
        else
          format.html { redirect_to transporters_dashboard_path, notice: 'Trip was successfully created.' }
        end
      else
        format.html { render :new }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @trip.update(trip_params)
        if current_user.super_user?
          format.html { redirect_to super_users_dashboard_path, notice: 'Trip was successfully updated.' }
        else
          format.html { redirect_to transporters_dashboard_path, notice: 'Trip was successfully updated.' }
        end
      else
        format.html { render :edit }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @trip.destroy
    respond_to do |format|
      if current_user.super_user?
        format.html { redirect_to super_users_dashboard_path, notice: 'Trip was successfully destroyed.' }
      else
        format.html { redirect_to transporters_dashboard_path, notice: 'Trip was successfully destroyed.' }
      end
    end
  end

  private
    def set_trip
      @trip = Trip.find(params[:id])
    end

    def trip_params
      params.require(:trip).permit(:weight, :type_of_truck, :contact, :arrival_hours, :depart_hours, :departure_city, :arrival_city, :pick_up_date, :delivery_date)
    end
end