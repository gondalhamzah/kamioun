class FreightMissionsController < ApplicationController
  before_action :set_freight_mission, only: [:show, :edit, :update, :destroy]

  def index
    @freight_missions = current_user.freight_missions
  end

  def show
  end

  def new
    @freight_mission = FreightMission.new
  end

  def edit
  end

  def create
    @freight_mission = current_user.freight_missions.new(freight_mission_params)
    respond_to do |format|
      if @freight_mission.save
        SendSmsWorker.perform_async
        if current_user.super_user?
          format.html { redirect_to super_users_dashboard_path, notice: 'Freight mission was successfully created.' }
        else
          format.html { redirect_to clients_dashboard_path, notice: 'Freight mission was successfully created.' }
        end
      else
        format.html { render :new }
        format.json { render json: @freight_mission.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @freight_mission.update(freight_mission_params)
        if current_user.super_user?
          format.html { redirect_to super_users_dashboard_path, notice: 'Freight mission was successfully updated.' }
        else
          format.html { redirect_to clients_dashboard_path, notice: 'Freight mission was successfully updated.' }
        end
      else
        format.html { render :edit }
        format.json { render json: @freight_mission.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @freight_mission.destroy
    respond_to do |format|
      if current_user.super_user?
        format.html { redirect_to super_users_dashboard_path, notice: 'Freight mission was successfully destroyed.' }
      else
        format.html { redirect_to clients_dashboard_path, notice: 'Freight mission was successfully destroyed.' }
      end
    end
  end

  private
    def set_freight_mission
      @freight_mission = FreightMission.find(params[:id])
    end

    def freight_mission_params
      params.require(:freight_mission).permit(:departure_city, :arrival_city, :type_of_goods, :dimensions, :pick_up_date, :pick_up_time_min, :pick_up_time_max, :delivery_date, :delivery_time_min, :delivery_time_max, :specific_details, :phone)
    end
end
