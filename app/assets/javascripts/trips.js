$(document).ready(function (){
  $('.timepicker-mission').datetimepicker({
    format: 'HH:mm',
    ignoreReadonly: true
  });

  $('.datepicker-mission').datetimepicker({
    format: 'DD/MM/YYYY',
    ignoreReadonly: true
  });

  $('#submitbtntrip').attr('disabled','disabled');

  function validation_check(){
    var min_pick_time = $("#trip_depart_hours");
    var max_pick_time = $("#trip_arrival_hours");

    var pick_date = $("#trip_delivery_date");
    var delivery_date = $("#trip_pick_up_date");

    var arrival_city = $('#trip_arrival_city');
    var departure_city = $('#trip_departure_city');
    var trip_contact = $('#trip_contact');
    var trip_type_of_truck = $('#trip_type_of_truck');
    var trip_weigth = $('#trip_weigth');

    if (trip_weigth.val() != "" && trip_type_of_truck.val() != "" && trip_contact.val() != "" && departure_city.val() != "" && arrival_city.val() != "" && min_pick_time.val() != "" && max_pick_time.val() != "" && pick_date.val() != "" && delivery_date.val() != "" ){
      $('#submitbtntrip').removeAttr("disabled");
    }else{
      $('#submitbtntrip').attr('disabled','disabled');
    }
  }

  $(document).on('dp.change', 'input.timepicker-mission', function() {
    validation_check();
  });

  $(document).on('dp.change', 'input.datepicker-mission', function() {
    validation_check();
  });

  $('#trip_arrival_city ').on('input change', function() { 
      validation_check();
  });

  $('#trip_departure_city').on('input change', function() { 
      validation_check();
  });
  
  $('#trip_contact').on('input change', function() { 
      validation_check();
  });
  
  $('#trip_type_of_truck').on('input change', function() { 
      validation_check();
  });
  
  $('#trip_weight').on('input change', function() { 
      validation_check();
  });

});