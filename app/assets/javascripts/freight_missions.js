$(document).ready(function (){
  $('.timepicker-mission').datetimepicker({
    format: 'HH:mm',
    ignoreReadonly: true
  });

  $('.datepicker-mission').datetimepicker({
    format: 'DD/MM/YYYY',
    ignoreReadonly: true
  });

// Code for error messages in the form
/*  $('#submitbtn').click(function(){
    $('.delivery_time_min-error').addClass("hide");
    $('.pick_time_min-error').addClass("hide");
    $('.delivery_date-error').addClass("hide");
    $('.pick_time_max-error').addClass("hide");
    $('.delivery_time_max-error').addClass("hide");
    $('.phone-error').addClass("hide");
    $('.goods-error').addClass("hide");
    $('.dimension-error').addClass("hide");
    $('.city-error').addClass("hide");
    $('.depart-error').addClass("hide");
    $('.pick_date-error ').addClass("hide");

    var form_validator = false;
    var min_pick_time = $("#freight_mission_pick_up_time_min");
    var max_pick_time = $("#freight_mission_pick_up_time_max");

    var min_delivery_time = $("#freight_mission_delivery_time_min");
    var max_delivery_time = $(".delivery_max");

    var pick_date = $("#freight_mission_pick_up_date");
    var delivery_date = $("#freight_mission_delivery_date");
    
    if (max_delivery_time.val() == ""){
      $('.delivery_time_max-error').removeClass("hide");
      $('.delivery_time_max-error').text("*cela ne peut pas être vide");
      form_validator = true;
      
    }else{
      if (max_delivery_time.val() < min_delivery_time.val()){
        $('.delivery_time_max-error').text("*la date de livraison devrait être supérieure à la date de la collecte");
        $('.delivery_time_max-error').removeClass("hide");
        form_validator = true;
      }else{
      }
    }

    if ($("#freight_mission_pick_up_time_min").val() == ""){
      $('.pick_time_min-error').removeClass("hide");
      form_validator = true;
    
    }
    if ($("#freight_mission_pick_up_time_max").val() == ""){
      $('.pick_time_max-error').removeClass("hide");
      $('.pick_time_max-error').text("*cela ne peut pas être vide");
      form_validator = true;
    
    }else{
      if (max_pick_time.val() < min_pick_time.val()){
        $('.pick_time_max-error').text("*devrait être supérieur à l'heure min");
        $('.pick_time_max-error').removeClass("hide");
        form_validator = true;
      }else{
      }
    }
    if ($("#freight_mission_phone").val() == ""){
      $('.phone-error').removeClass("hide");
      form_validator = true;
    }
    if ($("#freight_mission_type_of_goods").val() == ""){
      $('.goods-error').removeClass("hide");
      form_validator = true;
    }
    if ($("#freight_mission_dimensions").val() == ""){
      $('.dimension-error').removeClass("hide");
      form_validator = true;
    }
    if ($("#freight_mission_arrival_city").val() == ""){
      $('.city-error').removeClass("hide");
      form_validator = true;
    }
    if ($("#freight_mission_departure_city").val() == ""){
      $('.depart-error').removeClass("hide");
      form_validator = true;
    }
    if ($("#freight_mission_pick_up_date").val() == ""){
      $('.pick_date-error').removeClass("hide");
      form_validator = true;
    }
    if ($("#freight_mission_delivery_time_min").val() == ""){
      $('.delivery_time_min-error').removeClass("hide");
      form_validator = true;
    }
    if ($("#freight_mission_delivery_date").val() == ""){
      $('.delivery_date-error').removeClass("hide");
      $('.delivery_date-error').text("*cela ne peut pas être vide");
      form_validator = true;
    }else{
      if (delivery_date.val() < pick_date.val()){
        $('.delivery_date-error').text("*la date de livraison devrait être supérieure à la date de la collecte");
        $('.delivery_date-error').removeClass("hide");
        form_validator = true;
      }
    }
    if (form_validator == false){
      $("#new_freight_mission").submit();
    }
  });
*/
  
  $('#submitbtn').attr('disabled','disabled');

  function validation_check(){
    var min_pick_time = $("#freight_mission_pick_up_time_min");
    var max_pick_time = $("#freight_mission_pick_up_time_max");

    var min_delivery_time = $("#freight_mission_delivery_time_min");
    var max_delivery_time = $(".delivery_max");

    var pick_date = $("#freight_mission_pick_up_date");
    var delivery_date = $("#freight_mission_delivery_date");

    var arrival_city = $('#freight_mission_arrival_city');
    var departure_city = $('#freight_mission_departure_city');
    var phone = $('#freight_mission_phone');
    var type_of_goods = $('#freight_mission_type_of_goods');
    var dimensions = $('#freight_mission_dimensions');

    if (dimensions.val() != "" && type_of_goods.val() != "" && phone.val() != "" && departure_city.val() != "" && arrival_city.val() != "" && min_pick_time.val() != "" && min_delivery_time.val() != "" && max_pick_time.val() != "" && max_delivery_time.val() != "" && pick_date.val() != "" && delivery_date.val() != "" ){
      $('#submitbtn').removeAttr("disabled");
    }else{
      $('#submitbtn').attr('disabled','disabled');
    }
  }

  $(document).on('dp.change', 'input.timepicker-mission', function() {
    validation_check();
  });

  $(document).on('dp.change', 'input.datepicker-mission', function() {
    validation_check();
  });

  $('#freight_mission_arrival_city ').on('input', function() { 
      validation_check();
  });

  $('#freight_mission_departure_city').on('input', function() { 
      validation_check();
  });
  
  $('#freight_mission_phone').on('input', function() { 
      validation_check();
  });
  
  $('#freight_mission_type_of_goods').on('input', function() { 
      validation_check();
  });
  
  $('#freight_mission_dimensions').on('input', function() { 
      validation_check();
  });

  
  $('.goods').on('change', function() { 
    inputs = $('.goods');
    if($(inputs[0]).val() == ""){
      $(inputs[0]).val($(inputs[1]).val());
    
    }else if($(inputs[1]).val() == ""){
      $(inputs[1]).val($(inputs[0]).val());
    }
  });

  $('.dimensions').on('change', function() { 
    inputs = $('.dimensions');
    if($(inputs[0]).val() == ""){
      $(inputs[0]).val($(inputs[1]).val());
    
    }else if($(inputs[1]).val() == ""){
      $(inputs[1]).val($(inputs[0]).val());
    }
  });

  $('.arrival_city').on('change', function() { 
    inputs = $('.arrival_city');
    if($(inputs[0]).val() == ""){
      $(inputs[0]).val($(inputs[1]).val());
    
    }else if($(inputs[1]).val() == ""){
      $(inputs[1]).val($(inputs[0]).val());
    }
  });

});