ActiveAdmin.register Client do

  permit_params :first_name, :last_name, :email, :password, :password_confirmation, :company

 controller do
    def create
      params.permit!
      user = Client.new(params[:client])
      if user.save
        redirect_to admin_clients_path
      else
        redirect_to new_admin_client
      end
    end
  end

  form title: 'Add Client'  do |f|
    inputs "Client Details" do
      input :first_name, label: "Client's First Name"
      input :last_name , label: "Client's Last Name"
      input :email, label: "Email"
      input :password , label: "Password"
      input :password_confirmation , label: "Confirm Password"
      input :company , label: "Company"
    end
    actions
  end
    
  index do
    column :first_name
    column :last_name
    column :email
    column :company
    column :type
    actions
  end

end