ActiveAdmin.register Transporter do

  permit_params :first_name, :last_name, :email, :password, :password_confirmation, :company, :phone

 controller do
    def create
      params.permit!
      user = Transporter.new(params[:transporter])
      if user.save
        redirect_to admin_transporters_path
      else
        redirect_to new_admin_transporter 
      end
    end
  end

  form title: 'Add Transporter'  do |f|
    inputs "Transporter Details" do
      input :first_name, label: "Transporter's First Name"
      input :last_name , label: "Transporter's Last Name"
      input :email, label: "Email"
      input :password, label: "Password"
      input :password_confirmation , label: "Confirm Password"
      input :company, label: "Company"
      input :phone, label: "Phone"
    end
    actions
  end
    
  index do
    column :first_name
    column :last_name
    column :email
    column :company
    column :type
    column :phone
    actions
  end

end