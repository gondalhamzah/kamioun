ActiveAdmin.register SuperUser do

  permit_params :first_name, :last_name, :email, :password, :password_confirmation, :company, :phone

 controller do
    def create
      params.permit!
      user = SuperUser.new(params[:super_user])
      if user.save
        redirect_to admin_super_users_path
      else
        redirect_to new_admin_super_user_path
      end
    end
  end

  form title: 'Add Super User'  do |f|
    inputs "Super User Details" do
      input :first_name, label: "SuperUser's First Name"
      input :last_name , label: "SuperUser's Last Name"
      input :email, label: "Email"
      input :password , label: "Password"
      input :password_confirmation , label: "Confirm Password"
      input :company , label: "Company"
      input :phone , label: "Phone"

    end
    actions
  end
    
  index do
    column :first_name
    column :last_name
    column :email
    column :company
    column :type
    column :phone
    actions
  end

end