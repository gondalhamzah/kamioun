class AddSuperUserIdToMissions < ActiveRecord::Migration[5.1]
  def change
    rename_column :freight_missions, :client_id, :user_id
  end
end
