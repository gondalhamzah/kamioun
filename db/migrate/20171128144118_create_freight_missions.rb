class CreateFreightMissions < ActiveRecord::Migration[5.1]
  def change
    create_table :freight_missions do |t|
      t.string :departure_city
      t.string :arrival_city
      t.string :type_of_goods
      t.string :dimensions
      t.date :pick_up_date
      t.date :delivery_date
      t.text :specific_details

      t.timestamps
    end
  end
end
