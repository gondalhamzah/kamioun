class CreateTrips < ActiveRecord::Migration[5.1]
  def change
    create_table :trips do |t|
      t.string :weight
      t.string :type_of_truck
      t.string :contact
      t.string :arrival_hours
      t.string :depart_hours
      t.string :departure_city
      t.string :arrival_city
      t.integer :user_id

      t.timestamps
    end
  end
end
