class AddFieldsToMissions < ActiveRecord::Migration[5.1]
  def change
    add_column :freight_missions, :phone, :string
    add_column :freight_missions, :pick_up_time_max, :string
    add_column :freight_missions, :delivery_time_max, :string
    add_column :freight_missions, :pick_up_time_min, :string
    add_column :freight_missions, :delivery_time_min, :string
    add_column :freight_missions, :client_id, :integer
  end
end

