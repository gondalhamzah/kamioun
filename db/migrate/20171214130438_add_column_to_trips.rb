class AddColumnToTrips < ActiveRecord::Migration[5.1]
  def change
    add_column :trips, :pick_up_date, :date
    add_column :trips, :delivery_date, :date
  end
end